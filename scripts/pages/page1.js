const extend = require("js-base/core/extend");
const Button = require('sf-core/ui/button');
const Color = require("sf-core/ui/color");
const FlexLayout = require('sf-core/ui/flexlayout');
const Font = require('sf-core/ui/font');
const Image = require('sf-core/ui/image');
const ImageFillType = require('sf-core/ui/imagefilltype');
const ImageView = require('sf-core/ui/imageview');
const Page = require("sf-core/ui/page");
const Label = require("sf-core/ui/label");
const Router = require("sf-core/ui/router");
const TextAlignment = require('sf-core/ui/textalignment');

// Get generetad UI code
var Page1Design = require("../ui/ui_page1");

const getCombinedStyle = require("library/styler-builder").getCombinedStyle;

const Page1 = extend(Page)(
    function(_super) {
        var self = this;
        _super(self);
        
//         const imgStyle = getCombinedStyle(".imageView", {
// 			left: null,
// 			top: null,
// 			right: 0,
// 			bottom: 0,
// 			height: 60.3,
// 			image: Image.createFromFile("images://smartface.png"),
// 			imageFillType: ImageFillType.ASPECTFIT,
// 			borderColor: Color.create(255, 255, 255, 255),
// 			borderWidth: 0,
// 			borderRadius: 0,
// 			visible: true,
// 			alpha: 1,
// 			width: null,
// 			marginLeft: 10,
// 			marginRight: 10,
// 			positionType: FlexLayout.PositionType.RELATIVE,
// 			alignSelf: FlexLayout.AlignSelf.STRETCH
// 		});  
// 		var img = new ImageView(imgStyle);  
// 		this.layout.addChild(img);
        
        const lblStyle = getCombinedStyle(".label", {
			left: null,
			top: null,
			right: null,
			bottom: null,
			height: 90.45000000000002,
			backgroundColor: Color.create(0, 255, 255, 255),
			borderColor: Color.create(255, 255, 255, 255),
			borderRadius: 0,
			borderWidth: 0,
			visible: true,
			textAlignment: TextAlignment.MIDCENTER,
			alpha: 1,
			width: null,
			text: "Hello World",
			marginLeft: 10,
			marginRight: 10,
			multiline: true,
			positionType: FlexLayout.PositionType.RELATIVE,
			alignSelf: FlexLayout.AlignSelf.STRETCH,
			font: Font.create("Arial", 16, Font.NORMAL)
		});
		var lbl = new Label(lblStyle);
		if(lblStyle.scrollEnabled === false)
			lbl.ios && (lbl.ios.scrollEnabled = false);
		this.layout.addChild(lbl);
		
// 		const flexlayoutStyle = getCombinedStyle(".flexLayout", {
// 			right: null,
// 			bottom: null,
// 			height: 130,
// 			backgroundColor: Color.create(0, 255, 255, 255),
// 			borderColor: Color.create(255, 0, 0, 0),
// 			borderWidth: 0,
// 			visible: true,
// 			alpha: 1,
// 			width: null,
// 			left: null,
// 			marginLeft: 10,
// 			marginRight: 10,
// 			alignContent: FlexLayout.AlignContent.STRETCH,
// 			alignItems: FlexLayout.AlignItems.STRETCH,
// 			justifyContent: FlexLayout.JustifyContent.FLEX_START,
// 			flexWrap: FlexLayout.FlexWrap.NOWRAP,
// 			flexDirection: FlexLayout.FlexDirection.COLUMN,
// 			positionType: FlexLayout.PositionType.RELATIVE,
// 			alignSelf: FlexLayout.AlignSelf.STRETCH
// 		});  
// 		var flexlayout = new FlexLayout(flexlayoutStyle);  
// 		this.layout.addChild(flexlayout);
		
// 		const btnStyle = getCombinedStyle(".button", {
// 			left: null,
// 			top: null,
// 			right: 0,
// 			bottom: 0,
// 			height: 60,
// 			text: "Click me!",
// 			backgroundColor: Color.create(255, 0, 161, 241),
// 			borderColor: Color.create(255, 255, 255, 255),
// 			borderRadius: 0,
// 			borderWidth: 0,
// 			visible: true,
// 			alpha: 1,
// 			width: null,
// 			positionType: FlexLayout.PositionType.RELATIVE,
// 			alignSelf: FlexLayout.AlignSelf.STRETCH,
// 			font: Font.create("Arial", 16, Font.NORMAL)
// 		});  
// 		var btn = new Button(btnStyle);  
// 		flexlayout.addChild(btn);
		
// 		const btnNextStyle = getCombinedStyle(".button", {
// 			left: null,
// 			top: 10,
// 			right: 0,
// 			bottom: 0,
// 			height: 60,
// 			text: "Next Page",
// 			backgroundColor: Color.create(255, 0, 161, 241),
// 			borderColor: Color.create(255, 255, 255, 255),
// 			borderWidth: 0,
// 			borderRadius: 0,
// 			visible: true,
// 			alpha: 1,
// 			width: null,
// 			positionType: FlexLayout.PositionType.RELATIVE,
// 			alignSelf: FlexLayout.AlignSelf.STRETCH,
// 			font: Font.create("Arial", 16, Font.NORMAL)
// 		});  
// 		var btnNext = new Button(btnNextStyle);  
// 		flexlayout.addChild(btnNext);
// 		this.btnNext = btnNext;
		
        // this.headerBar.leftItemEnabled = false;
        // this.children.flexlayout.children.btn.onPress = btn_onPress.bind(this);
        // this.btnNext.onPress = btnNext_onPress.bind(this);
    });

function btnNext_onPress() {
    Router.go("page2", {
        message: "Hello World!"
    });
}

var btnClickCount = 0;

// Gets/sets press event callback for btn
function btn_onPress() {
    var myLabelText = "";
    var myButtonText = "";

    btnClickCount += 1;

    switch (true) {
        case btnClickCount == 1:
            myLabelText = "Well Done! \nYou've clicked the button!";
            myButtonText = "Click me again!";
            break;
        case btnClickCount < 10:
            myLabelText = "Whoa!\nThat click was " + numberSuffix(btnClickCount) + " time!";
            myButtonText = "Click again?";
            break;
        case btnClickCount < 15:
            myLabelText = "Feel tired?\nYou can rest your finger now :)";
            myButtonText = "I'm not tired!";
            break;
        default:
            myLabelText = "Isn't it good?\nEvery clicks count, you've clicked " + numberSuffix(btnClickCount) + " time!";
            myButtonText = "Click again?";
            break;
    }

    // Access lbl & btnNext of page1
    this.children.lbl.text = myLabelText;
    this.children.flexlayout.children.btn.text = myButtonText;
}

// Adds appropriate suffix to given number
function numberSuffix(number) {
    var suffix = "th";

    //Lets deal with small numbers
    var smallNumber = number % 100;

    if (smallNumber < 11 || smallNumber > 13) {
        switch (smallNumber % 10) {
            case 1:
                suffix = 'st';
                break;
            case 2:
                suffix = 'nd';
                break;
            case 3:
                suffix = 'rd';
                break;
        }
    }
    return number + suffix;
}

module && (module.exports = Page1);
